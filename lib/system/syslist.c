
#include <system/syslist.h>
#include <uapi/errno.h>

#if defined(TARGET_LINUX32) || defined(TARGET_LINUX64)

static void slists_lock_init(slists_lock_t *lock)
{
	if (lock) {
		pthread_mutex_init(lock, NULL);
	}
}

static void slists_lock(slists_lock_t *lock)
{
	if (lock) {
		pthread_mutex_lock(lock);
	}
}

static void slists_unlock(slists_lock_t *lock)
{
	if (lock) {
		pthread_mutex_unlock(lock);
	}
}

static void slists_lock_deinit(slists_lock_t *lock)
{
	if (lock) {
		pthread_mutex_destroy(lock);
	}
}

#elif defined(__XCC__) && defined(XOS_MUTEX_LOCK_USED)

static void slists_lock_init(slists_lock_t *lock)
{
	xos_mutex_create(lock, XOS_MUTEX_WAIT_PRIORITY, 0);
}

static void slists_lock(slists_lock_t *lock)
{
	if (lock) {
		xos_mutex_lock(lock);
	}
}

static void slists_unlock(slists_lock_t *lock)
{
	if (lock) {
		xos_mutex_unlock(lock);
	}
}

static void slists_lock_deinit(slists_lock_t *lock)
{
	if (lock) {
		xos_mutex_delete(lock);
	}
}

#else

#include <arch_irq.h>

static void slists_lock_init(slists_lock_t *lock)
{

}

static void slists_lock(slists_lock_t *lock)
{
	if (lock) {
		local_irq_save((*lock));
	}
}

static void slists_unlock(slists_lock_t *lock)
{
	if (lock) {
		local_irq_restore((*lock));
	}
}

static void slists_lock_deinit(slists_lock_t *lock)
{

}

#endif


/**
 *  slists_add
 */
static int slists_add(pslists_t psl, int bfree)
{
    plist_head_t node, head;
    pslists_group_t psg;

    if ( !psl )
        return -EPERM;
    psg = psl->group;
    if ( !psg )
        return -EPERM;

    slists_lock(&(psg->lock));

    node = &(psl->node);
    if (bfree) {
        head = &(psg->free);
    } else {
        head = &(psg->group);
    }

    list_del_init(node);
    list_add_tail(node, head);

    slists_unlock(&(psg->lock));

    return 0;

}

/**
 *  slists_pop
 */
static pslists_t slists_pop(pslists_group_t psg, int bfree)
{
    pslists_t  psl;
    plist_head_t node, head;

    if ( !psg )
        return NULL;

    slists_lock(&(psg->lock));

    if (bfree) {
        head = &(psg->free);
    } else {
        head = &(psg->group);
    }

    if ( list_empty_careful(head) ) {
        slists_unlock(&(psg->lock));
        return NULL;
    }

    psl = list_first_entry(head, slists_t, node);
    node = &(psl->node);
    list_del_init(node);

    slists_unlock(&(psg->lock));

    return psl;

}

/**
 *  slists_pop
 */
static pslists_t slists_top(pslists_group_t psg, int bfree)
{
    pslists_t  psl;
    plist_head_t head;

    if ( !psg )
        return NULL;

    slists_lock(&(psg->lock));

    if (bfree) {
        head = &(psg->free);
    } else {
        head = &(psg->group);
    }

    if ( list_empty_careful(head) ) {
        slists_unlock(&(psg->lock));
        return NULL;
    }

    psl = list_first_entry(head, slists_t, node);

    slists_unlock(&(psg->lock));

    return psl;

}

/**
 *  slists_count
 */
static int slists_count(pslists_group_t psg, int bfree)
{
    int count = 0;
    plist_head_t node, n, head;

    if ( !psg )
        return -1;

    slists_lock(&(psg->lock));

    if (bfree) {
        head = &(psg->free);
    } else {
        head = &(psg->group);
    }

    if ( list_empty_careful(head) ) {
        slists_unlock(&(psg->lock));
        return 0;
    }

    list_for_each_safe(node, n, head)
    {
        count++;
    }

    slists_unlock(&(psg->lock));

    return count;

}

/**
 * slists_group_init
 */
void slists_group_init(pslists_group_t psg)
{
    if (psg == NULL)
    	return;

    slists_lock_init(&(psg->lock));

    slists_lock(&(psg->lock));
    INIT_LIST_HEAD(&(psg->group));
    INIT_LIST_HEAD(&(psg->free));
    slists_unlock(&(psg->lock));

}

/**
 * slists_group_deinit
 */
void slists_group_deinit(pslists_group_t psg)
{
    if (psg == NULL)
    	return;

    slists_lock_deinit(&(psg->lock));

}

/**
 * slists_init
 */
void slists_init(pslists_t psl)
{
    if (psl == NULL)
    	return;
    INIT_LIST_HEAD(&(psl->node));
    psl->group = NULL;
}

/**
 *  slists_free
 */
int slists_free(pslists_t psl)
{
    return slists_add(psl, 1);
}

/**
 *  slists_put
 */
int slists_put(pslists_t  psl)
{
    return slists_add(psl, 0);
}

/**
 *  slists_alloc
 */
pslists_t slists_alloc(pslists_group_t psg)
{
    return slists_pop(psg, 1);
}

/**
 *  slists_get
 */
pslists_t slists_get(pslists_group_t psg)
{
    return slists_pop(psg, 0);
}

/**
 *  slists_chktop
 */
pslists_t slists_chktop(pslists_group_t psg)
{
    return slists_top(psg, 0);
}

/**
 *  slists_freenum
 */
int slists_freenum(pslists_group_t psg)
{
    return slists_count(psg, 1);
}

/**
 *  slists_topnum
 */
int slists_topnum(pslists_group_t psg)
{
    return slists_count(psg, 0);
}
