#ifndef _ARCH_CACHE_H
#define _ARCH_CACHE_H

#include <config.h>

#ifdef CONFIG_SYS_CACHE
#include <asm/cache.h>
#endif

#ifndef __ASSEMBLY__
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#ifdef CONFIG_SYS_CACHE

/**
 * 32 bits cache function -- cache_wback_inv
 */
#define dma_cache_wback_inv(addr, size)		_dma_cache_wback_inv((phys_addr_t)(addr), (phys_size_t)(size))

/**
 * 32 bits cache function -- cache_wback
 */
#define dma_cache_wback(addr, size)		_dma_cache_wback((phys_addr_t)(addr), (phys_size_t)(size))

/**
 * 32 bits cache function -- cache_inv
 */
#define dma_cache_inv(addr, size)		_dma_cache_inv((phys_addr_t)(addr), (phys_size_t)(size))

/**
 * 32 bits cache function -- dma_cache_invall
 */
#define dma_cache_invall()		_dma_cache_invall()

#else

/**
 * 32 bits cache function -- cache_wback_inv
 */
#define dma_cache_wback_inv(addr, size)	do{;}while(0)

/**
 * 32 bits cache function -- cache_wback
 */
#define dma_cache_wback(addr, size)	do{;}while(0)

/**
 * 32 bits cache function -- cache_inv
 */
#define dma_cache_inv(addr, size)	do{;}while(0)

/**
 * 32 bits cache function -- dma_cache_invall
 */
#define dma_cache_invall()	do{;}while(0)

#endif

#ifdef __cplusplus
}
#endif  /* __cplusplus */
#endif /*  __ASSEMBLY__ */

#endif /* _ARCH_CACHE_H */
