#ifndef _ARCH_TYPES_H
#define _ARCH_TYPES_H

#ifndef __ASSEMBLY__

#include <config.h>
#include <asm/types.h>
#include <uapi/endian.h>

#ifndef __PTRDIFF_TYPE__
#define __PTRDIFF_TYPE__ long int
#endif
typedef __PTRDIFF_TYPE__ ptrdiff_t;

#ifndef offsetof
#define offsetof(type, member) \
    ((size_t)( (char *)&(((type *)0)->member) - (char *)0 ))
#endif

#ifndef  container_of
# define container_of(ptr, type, member) (type *)((char*)ptr-offsetof(type,member))
#endif

#ifndef NOMINMAX

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#endif  /* NOMINMAX */

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

#endif /*  __ASSEMBLY__ */

#endif /* _ARCH_TYPES_H */
