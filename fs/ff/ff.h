/*----------------------------------------------------------------------------/
/  FatFs - Generic FAT Filesystem module  R0.14                               /
/-----------------------------------------------------------------------------/
/
/ Copyright (C) 2019, ChaN, all right reserved.
/
/ FatFs module is an open source software. Redistribution and use of FatFs in
/ source and binary forms, with or without modification, are permitted provided
/ that the following condition is met:

/ 1. Redistributions of source code must retain the above copyright notice,
/    this condition and the following disclaimer.
/
/ This software is provided by the copyright holder and contributors "AS IS"
/ and any warranties related to this software are DISCLAIMED.
/ The copyright owner or contributors be NOT LIABLE for any damages caused
/ by use of this software.
/
/----------------------------------------------------------------------------*/


#ifndef FF_DEFINED
#define FF_DEFINED	86606	/* Revision ID */

#ifdef __cplusplus
extern "C" {
#endif

/* Integer types used for FatFs API */

#if defined(_WIN32)	/* Main development platform */
#include <windows.h>
#elif (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L) || defined(__cplusplus)	/* C99 or later */
#include <stdint.h>
typedef unsigned int	FF_UINT;	/* int must be 16-bit or 32-bit */
typedef unsigned char	FF_BYTE;	/* char must be 8-bit */
typedef unsigned short	FF_WORD;	/* 16-bit unsigned integer */
typedef unsigned short	FF_WCHAR;	/* 16-bit unsigned integer */
typedef unsigned long	FF_DWORD;	/* 32-bit unsigned integer */
#elif defined(CONFIG_GCC)
#include <platform.h>
typedef unsigned int	FF_UINT;	/* int must be 16-bit or 32-bit */
typedef unsigned char	FF_BYTE;	/* char must be 8-bit */
typedef unsigned short	FF_WORD;	/* 16-bit unsigned integer */
typedef unsigned short	FF_WCHAR;	/* 16-bit unsigned integer */
typedef unsigned long	FF_DWORD;	/* 32-bit unsigned integer */
#else  	/* Earlier than C99 */
typedef unsigned int	FF_UINT;	/* int must be 16-bit or 32-bit */
typedef unsigned char	FF_BYTE;	/* char must be 8-bit */
typedef unsigned short	FF_WORD;	/* 16-bit unsigned integer */
typedef unsigned short	FF_WCHAR;	/* 16-bit unsigned integer */
typedef unsigned long	FF_DWORD;	/* 32-bit unsigned integer */
#endif

typedef FF_DWORD FSIZE_t;

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

/* ANSI/OEM code in SBCS/DBCS */
typedef char TCHAR;
#define _T(x) x
#define _TEXT(x) x

/* File function return code (FRESULT) */

typedef enum {
	FR_OK = 0,				/* (0) Succeeded */
	FR_DISK_ERR,			/* (1) A hard error occurred in the low level disk I/O layer */
	FR_INT_ERR,				/* (2) Assertion failed */
	FR_NOT_READY,			/* (3) The physical drive cannot work */
	FR_NO_FILE,				/* (4) Could not find the file */
	FR_NO_PATH,				/* (5) Could not find the path */
	FR_INVALID_NAME,		/* (6) The path name format is invalid */
	FR_DENIED,				/* (7) Access denied due to prohibited access or directory full */
	FR_EXIST,				/* (8) Access denied due to prohibited access */
	FR_INVALID_OBJECT,		/* (9) The file/directory object is invalid */
	FR_WRITE_PROTECTED,		/* (10) The physical drive is write protected */
	FR_INVALID_DRIVE,		/* (11) The logical drive number is invalid */
	FR_NOT_ENABLED,			/* (12) The volume has no work area */
	FR_NO_FILESYSTEM,		/* (13) There is no valid FAT volume */
	FR_MKFS_ABORTED,		/* (14) The f_mkfs() aborted due to any problem */
	FR_TIMEOUT,				/* (15) Could not get a grant to access the volume within defined period */
	FR_LOCKED,				/* (16) The operation is rejected according to the file sharing policy */
	FR_NOT_ENOUGH_CORE,		/* (17) LFN working buffer could not be allocated */
	FR_TOO_MANY_OPEN_FILES,	/* (18) Number of open files > FF_FS_LOCK */
	FR_INVALID_PARAMETER	/* (19) Given parameter is invalid */
} FRESULT;

/* File access mode and open method flags (3rd argument of f_open) */
#define	FA_READ				0x01
#define	FA_WRITE			0x02
#define	FA_OPEN_EXISTING	0x00
#define	FA_CREATE_NEW		0x04
#define	FA_CREATE_ALWAYS	0x08
#define	FA_OPEN_ALWAYS		0x10
#define	FA_OPEN_APPEND		0x30

/*--------------------------------------------------------------*/
typedef void *FIL;

/* FatFs module application interface                           */
FRESULT f_open (FIL* fp, const TCHAR* path, FF_BYTE mode);			/* Open or create a file */
FRESULT f_close (FIL* fp);											/* Close an open file object */
FRESULT f_read (FIL* fp, void* buff, FF_UINT btr, FF_UINT* br);			/* Read data from the file */
FRESULT f_write (FIL* fp, const void* buff, FF_UINT btw, FF_UINT* bw);	/* Write data to the file */
FRESULT f_lseek (FIL* fp, FSIZE_t ofs);								/* Move file pointer of the file object */
FRESULT f_sync (FIL* fp);											/* Flush cached data of the writing file */
int f_putc (TCHAR c, FIL* fp);										/* Put a character to the file */
int f_puts (const TCHAR* str, FIL* cp);								/* Put a string to the file */
int f_printf (FIL* fp, const TCHAR* str, ...);						/* Put a formatted string to the file */
TCHAR* f_gets (TCHAR* buff, int len, FIL* fp);						/* Get a string from the file */
int f_eof(FIL* fp);
int f_error(FIL* fp);
long f_tell(FIL *fp);
void f_rewind(FIL *fp);
long f_size(FIL *fp);

#ifndef EOF
#define EOF (-1)
#endif

#ifdef __cplusplus
}
#endif

#endif /* FF_DEFINED */
