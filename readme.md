# 嵌入式播放器/录像机  

## 使用前的准备  

* 代码中自行实现mp4/mkv/ts封装与解封装，使用嵌入式硬件编解码器或者libavcodec编解码器，使用嵌入式硬件显示器或者sdl2库，自行实现音视频同步  

* 使用Linux系统，我使用的是Ubuntu16.04，安装了32位运行库  

* 使用make menuconfig，要安装相应的ncurses依赖库  

* 仿真环境使用了ffmpeg的libavcodec编解码器代替嵌入式硬件编解码器，要安装ffmpeg  

* 仿真环境使用sdl2代替嵌入式硬件显示器，要安装sdl2  

* master分支是播放器，record分支是录像机

## 编译  

make menuconfig  
make  

* 编译日志如下：  
```c
jim@ubuntu:~/media$ make menuconfig
/home/jim/media/./scripts/hconfig/mconf Hconfig


*** End of the configuration.
*** Execute 'make' to start the build or try 'make help'.

jim@ubuntu:~/media$ make
  CHK     arch/xxx/cpu/include/asm/asm-offsets.h
  LDS     user/soc/xxx/xxx/xxx/player.lds
  ELF     user/soc/xxx/xxx/xxx/player.elf

```

## 运行  

./user/soc/xxx/xxx/xxx/player.elf  

* 运行日志如下：  

```c
jim@ubuntu:~/media$ ./user/soc/xxx/xxx/xxx/player.elf
SessionInit : sys ..
SessionInit : i2sout ..
SessionInit : xaaac ..
SessionInit : xamp3 ..
SessionInit : wav ..
SessionInit : display ..
SessionInit : malidecoder ..
SessionInit : tsha ..
SessionInit : mp4 ..
SessionInit : mkv ..


Switch to new stream 0
filename: /mnt/hgfs/video/xxx.mp4
Session 'mp4' command 'SSCMD_STREAM_START'
stream file size is 20080004

====>current video stream
width: 1280, height: 720
duration: 60s
TAG: avc1, len: 136
ctts(composition time, dts to pts)count: 1495

---->current audio stream
width: 0, height: 0
duration: 60s
TAG: mp4a, len: 90
audio channel count: 2, sample size: 16, sample rate: 44100
video pakcet duration: 40.000000ms, audio packet duration: 23.215683ms
Session 'malidecoder' command 'SSCMD_STREAM_START'
Session 'display' command 'SSCMD_STREAM_START'
Session 'xaaac' command 'SSCMD_STREAM_START'
Session 'i2sout' command 'SSCMD_STREAM_START'
Audio init, sample_rate: 44100, format: 0x8120, channels: 2
	Session mp4 set to SSTATE_RUNNING in SSCMD_STREAM_START
vpts: 2120, apts: 2205, va_diff: 33
vpts: 6640, apts: 6733, va_diff: 43
vpts: 10880, apts: 10959, va_diff: 59
vpts: 15520, apts: 15603, va_diff: 64
vpts: 20040, apts: 20108, va_diff: 41
vpts: 24520, apts: 24589, va_diff: 66
vpts: 28920, apts: 29001, va_diff: 42
vpts: 33600, apts: 33692, va_diff: 47
vpts: 38080, apts: 38173, va_diff: 63
vpts: 42520, apts: 42608, va_diff: 34
vpts: 47040, apts: 47159, va_diff: 27
vpts: 51440, apts: 51525, va_diff: 71
vpts: 55880, apts: 55960, va_diff: 64
Session 'malidecoder' command  'SSCMD_STREAM_STOP'
Session 'display' command  'SSCMD_STREAM_STOP'
Session 'xaaac' command  'SSCMD_STREAM_STOP'
Session 'i2sout' command  'SSCMD_STREAM_STOP'
Session 'mp4' command  'SSCMD_STREAM_STOP'
	Session mp4 set to SSTATE_STOP in SSCMD_STREAM_STOP

jim@ubuntu:~/media$ 

```
