
#ifndef _DECODER_SESSION_CMDID_H_
#define _DECODER_SESSION_CMDID_H_

//////////////////////////////////////////////////////////////////////////////////////////////
// public

typedef enum
{
	SSCMD_NONE = 0,
	SSCMD_STREAM_START,
	SSCMD_STREAM_PAUSE,
	SSCMD_STREAM_RESUME,
	SSCMD_STREAM_STOP,
	SSCMD_STREAM_SKIP,

	SSCMD_STREAM_BUFCHANGED,

	// speed
	SSCMD_STREAM_SETSPEED,
	SSCMD_STREAM_GETSPEED,

	// buffer
	SSCMD_STREAM_GETVALIDINSIZE,

	// session special command
	SSCMD_STREAM_USER = 0x10,
}SESSIONCMDID;

#define SSCMD_USER(x)	((x)+SSCMD_STREAM_USER)

#define SYSBUF_FLAG_TYPE_MASK	0xff

// speed
enum
{
	SESSION_SPEED_X1 = 1,
	SESSION_SPEED_X2 = 2,
	SESSION_SPEED_X4 = 4,
	SESSION_SPEED_X8 = 8,
	SESSION_SPEED_X16 = 16,
};

typedef unsigned int session_speed_t;

//////////////////////////////////////////////////////////////////////////////////////////////
// public: display
enum
{
	SSCMD_DISPLAY_LCDI = SSCMD_USER(0),
	SSCMD_DISPLAY_VIDEO_STREAM_CHANGED,
};

typedef struct
{
	unsigned int width;
	unsigned int height;
}video_stream_info_t;

//////////////////////////////////////////////////////////////////////////////////////////////
// public: i2sout

typedef struct
{
	unsigned int samplingrate;
	unsigned int samplingsize;
}audio_info_t;

enum
{
	SSCMD_SET_AUDIO_INFO = SSCMD_USER(0),
};

//////////////////////////////////////////////////////////////////////////////////////////////
// public: movie(MP4, MKV, TS)
enum
{
	SSCMD_STREAM_GETCURRENTPLAYTIME = SSCMD_USER(0),
};

//////////////////////////////////////////////////////////////////////////////////////////////
#endif // _DECODER_SESSION_CMDID_H_





