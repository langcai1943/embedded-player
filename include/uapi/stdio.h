/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1995, 1999, 2001, 2002 by Ralf Baechle
 */

#if defined(CONFIG_GCC)
#include <stdio.h>
#define _UAPI_STDIO_H
#endif

