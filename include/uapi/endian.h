#ifndef _UAPI_ENDIAN_H
#define _UAPI_ENDIAN_H

#include <uapi/features.h>

/* This file defines `__BYTE_ORDER' for the particular machine.  */
#include <uapi/bits/endian.h>

#if defined(_GNU_SOURCE) || defined(_BSD_SOURCE)

#undef LITTLE_ENDIAN
#undef BIG_ENDIAN

#include <uapi/bits/byteswap.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN

#define LITTLE_ENDIAN

# define htobe16(x) __bswap_16 (x)
# define be16toh(x) __bswap_16 (x)
# define betoh16(x) __bswap_16 (x)

# define htobe32(x) __bswap_32 (x)
# define be32toh(x) __bswap_32 (x)
# define betoh32(x) __bswap_32 (x)

# define htole16(x) (x)
# define le16toh(x) (x)
# define letoh16(x) (x)

# define htole32(x) (x)
# define le32toh(x) (x)
# define letoh32(x) (x)

#elif __BYTE_ORDER == __BIG_ENDIAN

#define BIG_ENDIAN

# define htobe16(x) (x)
# define be16toh(x) (x)
# define betoh16(x) (x)

# define htobe32(x) (x)
# define be32toh(x) (x)
# define betoh32(x) (x)

# define htole16(x) __bswap_16 (x)
# define le16toh(x) __bswap_16 (x)
# define letoh16(x) __bswap_16 (x)

# define htole32(x) __bswap_32 (x)
# define le32toh(x) __bswap_32 (x)
# define letoh32(x) __bswap_32 (x)

#else

#error __BYTE_ORDER not defined !

#endif

#endif

#endif
