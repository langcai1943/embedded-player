#ifndef _UAPI_NETINET_IN_SYSTM_H
#define _UAPI_NETINET_IN_SYSTM_H

#include <uapi/stdint.h>

typedef uint16_t n_short;
typedef uint32_t n_long, n_time;

#endif
