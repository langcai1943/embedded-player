/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1995, 1999, 2001, 2002 by Ralf Baechle
 */

#ifndef __UAPI_HBUILD_H
#define __UAPI_HBUILD_H

#include <stddef.h>

#define COMMENT(x) \
	asm volatile("\n->#" x)

#define DEFINE(sym, val) \
	asm volatile("\n->" #sym " %0 " #val : : "i" (val))

#define BLANK() \
	asm volatile("\n->" : : )

#define OFFSET(sym, str, mem) \
	DEFINE(sym, offsetof(str, mem))

#endif
 
