/* Determine the wordsize from the preprocessor defines.  */

#ifndef	_UAPI_WORDSIZE_H
#define	_UAPI_WORDSIZE_H	1

#ifndef __WORDSIZE

#if defined(__SIZEOF_INT__)

#if __SIZEOF_INT__ == 2
# define __WORDSIZE 16
#elif __SIZEOF_INT__ == 4
# define __WORDSIZE 32
#endif

#elif defined(__XCC__)
# define __WORDSIZE 32
#endif

#endif // !__WORDSIZE

#if !defined(__WORDSIZE)
# error __WORDSIZE must be defined !
#endif

#endif // bits/wordsize.h
