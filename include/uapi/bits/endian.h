
#ifndef _UAPI_BITS_ENDIAN_H
#define _UAPI_BITS_ENDIAN_H

#include <compiler.h>

//
// Endian
//
#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN     1234
#endif
#ifndef __BIG_ENDIAN
#define __BIG_ENDIAN        4321
#endif
#ifndef __PDP_ENDIAN
#define __PDP_ENDIAN        3412
#endif

#ifndef __BYTE_ORDER
#if defined (SDCC) || defined (__SDCC)
#if defined(__SDCC_hc08) || defined(__SDCC_s08) || defined(__SDCC_stm8)
# define __BYTE_ORDER    __BIG_ENDIAN
#else
# define __BYTE_ORDER    __LITTLE_ENDIAN
#endif
#elif defined (__CX51__) || defined (__C51__)
# define __BYTE_ORDER    __BIG_ENDIAN
#elif defined (__XCC__)
# define __BYTE_ORDER    __LITTLE_ENDIAN
#endif
#endif

#ifndef __BYTE_ORDER
#if defined(__GNUC__) && defined(__BYTE_ORDER__)
#define __BYTE_ORDER __BYTE_ORDER__
#else
# error __BYTE_ORDER must be defined !
#endif
#endif

#endif // bits/endian.h
