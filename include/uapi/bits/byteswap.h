/* Macros to swap the order of bytes in integer values.
   Copyright (C) 1997-2013 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _UAPI_BITS_BYTESWAP_H
#define _UAPI_BITS_BYTESWAP_H 1

#if defined(TARGET_LINUX32) || defined(TARGET_LINUX64)

#include <byteswap.h>

#else

#include <uapi/bits/types.h>

#ifndef __ASSEMBLY__

#ifdef __cplusplus
extern "C" {
#endif

#undef __bswap_16
extern uint16_t __bswap_16(uint16_t __x);

#undef __bswap_32
extern uint32_t __bswap_32(uint32_t __x);

#ifdef __cplusplus
}
#endif

#endif // __ASSEMBLY__

#endif // TARGET_LINUXxx

#define uswap_16(x) \
	((((x) & 0xff00) >> 8) | \
	 (((x) & 0x00ff) << 8))
#define uswap_32(x) \
	((((x) & 0xff000000) >> 24) | \
	 (((x) & 0x00ff0000) >>  8) | \
	 (((x) & 0x0000ff00) <<  8) | \
	 (((x) & 0x000000ff) << 24))

#endif /* bits/byteswap.h */
