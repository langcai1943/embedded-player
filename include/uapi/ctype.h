/* Copyright (C) 1991,92,93,95,96,97,98,99,2001,02
	Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#if defined(__XCC__)
#include <ctype.h>
#define _UAPI_CTYPE_H
#endif

/*
 *	ISO C99 Standard 7.4: Character handling	<ctype.h>
 */

#ifndef	_UAPI_CTYPE_H
#define	_UAPI_CTYPE_H	1

#include <uapi/features.h>
#include <uapi/bits/types.h>

__BEGIN_DECLS
__BEGIN_NAMESPACE_STD

extern int isalnum(unsigned char c);
extern int isalpha(unsigned char c);
extern int iscntrl(unsigned char c);
extern int isdigit(unsigned char c);
extern int islower(unsigned char c);
extern int isgraph(unsigned char c);
extern int isprint(unsigned char c);
extern int ispunct(unsigned char c);
extern int isspace(unsigned char c);
extern int isupper(unsigned char c);
extern int isxdigit(unsigned char c);

/*
 * Rather than doubling the size of the _ctype lookup table to hold a 'blank'
 * flag, just check for space or tab.
 */
#define isblank(c)  (c == ' ' || c == '\t')

#define isascii(c) (((unsigned char)(c))<=0x7f)
#define toascii(c) (((unsigned char)(c))&0x7f)

extern unsigned char __tolower(unsigned char c);
extern unsigned char __toupper(unsigned char c);

#define tolower(c)      __tolower(c)
#define toupper(c)      __toupper(c)

/* Fast check for octal digit */
extern int isodigit(unsigned char c);
#define isodigit(c) isodigit(c)

__END_NAMESPACE_STD
__END_DECLS

#endif /* _UAPI_CTYPE_H */
