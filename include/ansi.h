
#ifndef __ANSI_H__
#define __ANSI_H__

#define ANSI_NUL            0x00  // NUL(null)
#define ANSI_SOH            0x01  // SOH(start of headling)
#define ANSI_STX            0x02  // STX (start of text)
#define ANSI_ETX            0x03  // ETX (end of text)
#define ANSI_EOT            0x04  // EOT (end of transmission)
#define ANSI_ENQ            0x05  // ENQ (enquiry)
#define ANSI_ACK            0x06  // ACK (acknowledge)
#define ANSI_BEL            0x07  // BEL (bell)
#define ANSI_BS             0x08  // BS (backspace)
#define ANSI_HT             0x09  // HT (horizontal tab)
#define ANSI_LF             0x0a  // LF (NL line feed, new line)
#define ANSI_VT             0x0b  // VT (vertical tab)
#define ANSI_FF             0x0c  // FF (NP form feed, new page)
#define ANSI_CR             0x0d  // CR (carriage return)
#define ANSI_SO             0x0e  // SO (shift out)
#define ANSI_SI             0x0f  // SI (shift in)
#define ANSI_DLE            0x10  // DLE (data link escape)
#define ANSI_DC1            0x11  // DC1 (device control 1)
#define ANSI_DC2            0x12  // DC2 (device control 2)
#define ANSI_DC3            0x13  // DC3 (device control 3)
#define ANSI_DC4            0x14  // DC4 (device control 4)
#define ANSI_NAK            0x15  // NAK (negative acknowledge)
#define ANSI_SYN            0x16  // SYN (synchronous idle)
#define ANSI_ETB            0x17  // ETB (end of trans. block)
#define ANSI_CAN            0x18  // CAN (cancel)
#define ANSI_EM             0x19  // EM (end of medium)
#define ANSI_SUB            0x1a  // SUB (substitute)
#define ANSI_ESC            0x1b  // ESC (escape)
#define ANSI_FS             0x1c  // FS (file separator)
#define ANSI_GS             0x1d  // GS (group separator)
#define ANSI_RS             0x1e  // RS (record separator)
#define ANSI_US             0x1f  // US (unit separator)
#define ANSI_SP             0x20  // space

#define ANSI_BANG           0x21 // !
#define ANSI_QUOTE          0x22 // "
#define ANSI_HASH           0x23 // #
#define ANSI_DOLLAR         0x24 // $
#define ANSI_PERCENT        0x25 // %
#define ANSI_AND            0x26 // &
#define ANSI_APOSTROPHE     0x27 // '
#define ANSI_OPEN_PARENTH   0x28 // (
#define ANSI_CLOSE_PARENTH  0x29 // )
#define ANSI_ASTERISK       0x2a // *
#define ANSI_PLUS           0x2b // +
#define ANSI_COMMA          0x2c // ,
#define ANSI_DASH           0x2d // -
#define ANSI_DOT            0x2e // .
#define ANSI_WHACK          0x2f // /

#define ANSI_0              0x30 // 0
#define ANSI_1              0x31 // 1
#define ANSI_2              0x32 // 2
#define ANSI_3              0x33 // 3
#define ANSI_4              0x34 // 4
#define ANSI_5              0x35 // 5
#define ANSI_6              0x36 // 6
#define ANSI_7              0x37 // 7
#define ANSI_8              0x38 // 8
#define ANSI_9              0x39 // 9

#define ANSI_COLON          0x3a // :
#define ANSI_SEMICOLON      0x3b // ;
#define ANSI_LESS_THAN      0x3c // <
#define ANSI_EQUALS         0x3d // =
#define ANSI_GREATER_THAN   0x3e // >
#define ANSI_QUESTION_MARK  0x3f // ?
#define ANSI_AT             0x40 // @

#define ANSI_A              0x41 // A
#define ANSI_B              0x42 // B
#define ANSI_C              0x43 // C
#define ANSI_D              0x44 // D
#define ANSI_E              0x45 // E
#define ANSI_F              0x46 // F
#define ANSI_G              0x47 // G
#define ANSI_H              0x48 // H
#define ANSI_I              0x49 // I
#define ANSI_J              0x4a // J
#define ANSI_K              0x4b // K
#define ANSI_L              0x4c // L
#define ANSI_M              0x4d // M
#define ANSI_N              0x4e // N
#define ANSI_O              0x4f // O
#define ANSI_P              0x50 // P
#define ANSI_Q              0x51 // Q
#define ANSI_R              0x52 // R
#define ANSI_S              0x53 // S
#define ANSI_T              0x54 // T
#define ANSI_U              0x55 // U
#define ANSI_V              0x56 // V
#define ANSI_W              0x57 // W
#define ANSI_X              0x58 // X
#define ANSI_Y              0x59 // Y
#define ANSI_Z              0x5a // Z

#define ANSI_OPEN_BRACKET   0x5b // [
#define ANSI_BACK_SLASH     0x5c // '\'
#define ANSI_CLOSE_BRACKET  0x5d // ]
#define ANSI_CARET          0x5e // ^
#define ANSI_UNDERSCORE     0x5f // _
#define ANSI_ACCENT         0x60 // `

#define ANSI_a              0x61 // a
#define ANSI_b              0x62 // b
#define ANSI_c              0x63 // c
#define ANSI_d              0x64 // d
#define ANSI_e              0x65 // e
#define ANSI_f              0x66 // f
#define ANSI_g              0x67 // g
#define ANSI_h              0x68 // h
#define ANSI_i              0x69 // i
#define ANSI_j              0x6a // j
#define ANSI_k              0x6b // k
#define ANSI_l              0x6c // l
#define ANSI_m              0x6d // m
#define ANSI_n              0x6e // n
#define ANSI_o              0x6f // o
#define ANSI_p              0x70 // p
#define ANSI_q              0x71 // q
#define ANSI_r              0x72 // r
#define ANSI_s              0x73 // s
#define ANSI_t              0x74 // t
#define ANSI_u              0x75 // u
#define ANSI_v              0x76 // v
#define ANSI_w              0x77 // w
#define ANSI_x              0x78 // x
#define ANSI_y              0x79 // y
#define ANSI_z              0x7a // z

#define ANSI_OPEN_BRACE     0x7b // {
#define ANSI_OR             0x7c // |
#define ANSI_CLOSE_BRACE    0x7d // }
#define ANSI_TITLE          0x7e // ~
#define ANSI_DEL            0x7f // DEL

#define ANSI_PAD            0x80 // ESC+@	Padding Character
#define ANSI_HOP            0x81 // ESC+A	High Octet Preset
#define ANSI_BPH            0x82 // ESC+B	Break Permitted Here
#define ANSI_NBH            0x83 // ESC+C	No Break Here
#define ANSI_IND            0x84 // ESC+D	Index
#define ANSI_NEL            0x85 // ESC+E	Next Line
#define ANSI_SSA            0x86 // ESC+F	Start of Selected Area
#define ANSI_ESA            0x87 // ESC+G	End of Selected Area
#define ANSI_HTS            0x88 // ESC+H	Character Tabulation Set Horizontal Tabulation Set
#define ANSI_HTJ            0x89 // ESC+I	Character Tabulation With Justification Horizontal Tabulation With Justification
#define ANSI_VTS            0x8A // ESC+J	Line Tabulation Set Vertical Tabulation Set
#define ANSI_PLD            0x8B // ESC+K	Partial Line Forward Partial Line Down
#define ANSI_PLU            0x8C // ESC+L	Partial Line Backward Partial Line Up
#define ANSI_RI             0x8D // ESC+M	Reverse Line Feed Reverse Index
#define ANSI_SS2            0x8E // ESC+N	Single-Shift 2
#define ANSI_SS3            0x8F // ESC+O	Single-Shift 3
#define ANSI_DCS            0x90 // ESC+P	Device Control String
#define ANSI_PU1            0x91 // ESC+Q	Private Use 1
#define ANSI_PU2            0x92 // ESC+R	Private Use 2
#define ANSI_STS            0x93 // ESC+S	Set Transmit State
#define ANSI_CCH            0x94 // ESC+T	Cancel character
#define ANSI_MW             0x95 // ESC+U	Message Waiting
#define ANSI_SPA            0x96 // ESC+V	Start of Protected Area
#define ANSI_EPA            0x97 // ESC+W	End of Protected Area
#define ANSI_SOS            0x98 // ESC+X	Start of String
#define ANSI_SGCI           0x99 // ESC+Y	Single Graphic Character Introducer
#define ANSI_SCI            0x9A // ESC+Z	Single Character Introducer
#define ANSI_CSI            0x9B // ESC+[	Control Sequence Introducer
#define ANSI_ST             0x9C // ESC+\	String Terminator
#define ANSI_OSC            0x9D // ESC+]	Operating System Command
#define ANSI_PM             0x9E // ESC+^	Privacy Message
#define ANSI_APC            0x9F // ESC+_	Application Program Command

#endif // __ANSI_H__
