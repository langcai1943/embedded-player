 
#ifndef __ASM_GENERIC_DELAY_H
#define __ASM_GENERIC_DELAY_H

extern void udelay(long usecs);

#endif /* __ASM_GENERIC_DELAY_H */
