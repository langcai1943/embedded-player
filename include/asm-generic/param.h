#ifndef __ASM_GENERIC_PARAM_H
#define __ASM_GENERIC_PARAM_H

#ifndef HZ
#define HZ CONFIG_HZ
#endif

#define USER_HZ	CONFIG_SYS_HZ

#endif /* __ASM_GENERIC_PARAM_H */
