#ifndef _XORG_LIST_H_
#define _XORG_LIST_H_

#include <compiler.h>
#include <types.h>

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/**
* Returns a pointer to the container of this list element.
*
* Example:
* struct foo* f;
* f = container_of(&foo->entry, struct foo, entry);
* assert(f == foo);
*
* @param ptr Pointer to the struct list_head.
* @param type Data type of the list element.
* @param member Member name of the struct list_head field in the list element.
* @return A pointer to the data struct containing the list head.
*/

#ifndef container_of
#define container_of(ptr, type, member) \
    (type *)((char *)(ptr) - (char *) &((type *)0)->member)
#endif

/**
* The linkage struct for list nodes. This struct must be part of your
* to-be-linked struct. struct list_head is required for both the head of the
* list and for each list node.
*
* Position and name of the struct list_head field is irrelevant.
* There are no requirements that elements of a list are of the same type.
* There are no requirements for a list head, any struct list_head can be a list
* head.
*/

struct list_head {
	struct list_head *next;
	struct list_head *prev;
}PACKED;

typedef struct list_head list_head_t;
typedef struct list_head *plist_head_t ;

/*
* Simple doubly linked list implementation.
*
* Some of the internal functions ("__xxx") are useful when
* manipulating whole lists rather than single entries, as
* sometimes we already know the next/prev entries and we can
* generate better code by using them directly rather than
* using the generic single-entry routines.
*/

#define LIST_HEAD_INIT(name) { &(name), &(name) }

#define LIST_HEAD(name) \
	struct list_head name = LIST_HEAD_INIT(name)

extern void INIT_LIST_HEAD(struct list_head *list);

/**
* list_add - add a newl entry
* @newl: newl entry to be added
* @head: list head to add it after
*
* Insert a newl entry after the specified head.
* This is good for implementing stacks.
*/
extern void list_add(struct list_head *newl, struct list_head *head);


/**
* list_add_tail - add a newl entry
* @newl: newl entry to be added
* @head: list head to add it before
*
* Insert a newl entry before the specified head.
* This is useful for implementing queues.
*/
extern void list_add_tail(struct list_head *newl, struct list_head *head);

/**
* list_del - deletes entry from list.
* @entry: the element to delete from the list.
* Note: list_empty() on entry does not return true after this, the entry is
* in an undefined state.
*/
extern void list_del(struct list_head *entry);

/**
* list_replace - replace old entry by newl one
* @old : the element to be replaced
* @newl : the newl element to insert
*
* If @old was empty, it will be overwritten.
*/
extern void list_replace(struct list_head *old,
	struct list_head *newl);

extern void list_replace_init(struct list_head *old,
	struct list_head *newl);

/**
* list_swap - replace entry1 with entry2 and re-add entry1 at entry2's position
* @entry1: the location to place entry2
* @entry2: the location to place entry1
*/
extern void list_swap(struct list_head *entry1,
	struct list_head *entry2);

/**
* list_del_init - deletes entry from list and reinitialize it.
* @entry: the element to delete from the list.
*/
extern void list_del_init(struct list_head *entry);

/**
* list_move - delete from one list and add as another's head
* @list: the entry to move
* @head: the head that will precede our entry
*/
extern void list_move(struct list_head *list, struct list_head *head);

/**
* list_move_tail - delete from one list and add as another's tail
* @list: the entry to move
* @head: the head that will follow our entry
*/
extern void list_move_tail(struct list_head *list,
	struct list_head *head);

/**
* list_bulk_move_tail - move a subsection of a list to its tail
* @head: the head that will follow our entry
* @first: first entry to move
* @last: last entry to move, can be the same as first
*
* Move all entries between @first and including @last before @head.
* All three entries must belong to the same linked list.
*/
extern void list_bulk_move_tail(struct list_head *head,
	struct list_head *first,
	struct list_head *last);

/**
* list_is_first -- tests whether @list is the first entry in list @head
* @list: the entry to test
* @head: the head of the list
*/
extern int list_is_first(const struct list_head *list,
	const struct list_head *head);

/**
* list_is_last - tests whether @list is the last entry in list @head
* @list: the entry to test
* @head: the head of the list
*/
extern int list_is_last(const struct list_head *list,
	const struct list_head *head);

/**
* list_empty - tests whether a list is empty
* @head: the list to test.
*/
extern int list_empty(const struct list_head *head);

/**
* list_empty_careful - tests whether a list is empty and not being modified
* @head: the list to test
*
* Description:
* tests whether a list is empty _and_ checks that no other CPU might be
* in the process of modifying either member (next or prev)
*
* NOTE: using list_empty_careful() without synchronization
* can only be safe if the only activity that can happen
* to the list entry is list_del_init(). Eg. it cannot be used
* if another CPU could re-list_add() it.
*/
extern int list_empty_careful(const struct list_head *head);

/**
* list_rotate_left - rotate the list to the left
* @head: the head of the list
*/
extern void list_rotate_left(struct list_head *head);

/**
* list_rotate_to_front() - Rotate list to specific item.
* @list: The desired newl front of the list.
* @head: The head of the list.
*
* Rotates list so that @list becomes the newl front of the list.
*/
extern void list_rotate_to_front(struct list_head *list,
	struct list_head *head);

/**
* list_is_singular - tests whether a list has just one entry.
* @head: the list to test.
*/
extern int list_is_singular(const struct list_head *head);

/**
* list_cut_position - cut a list into two
* @list: a newl list to add all removed entries
* @head: a list with entries
* @entry: an entry within head, could be the head itself
*	and if so we won't cut the list
*
* This helper moves the initial part of @head, up to and
* including @entry, from @head to @list. You should
* pass on @entry an element you know is on @head. @list
* should be an empty list or a list you do not care about
* losing its data.
*
*/
extern void list_cut_position(struct list_head *list,
	struct list_head *head, struct list_head *entry);

/**
* list_cut_before - cut a list into two, before given entry
* @list: a newl list to add all removed entries
* @head: a list with entries
* @entry: an entry within head, could be the head itself
*
* This helper moves the initial part of @head, up to but
* excluding @entry, from @head to @list.  You should pass
* in @entry an element you know is on @head.  @list should
* be an empty list or a list you do not care about losing
* its data.
* If @entry == @head, all entries on @head are moved to
* @list.
*/
extern void list_cut_before(struct list_head *list,
	struct list_head *head,
	struct list_head *entry);

/**
* list_splice - join two lists, this is designed for stacks
* @list: the newl list to add.
* @head: the place to add it in the first list.
*/
extern void list_splice(const struct list_head *list,
	struct list_head *head);

/**
* list_splice_tail - join two lists, each list being a queue
* @list: the newl list to add.
* @head: the place to add it in the first list.
*/
extern void list_splice_tail(struct list_head *list,
	struct list_head *head);

/**
* list_splice_init - join two lists and reinitialise the emptied list.
* @list: the newl list to add.
* @head: the place to add it in the first list.
*
* The list at @list is reinitialised
*/
extern void list_splice_init(struct list_head *list,
	struct list_head *head);

/**
* list_splice_tail_init - join two lists and reinitialise the emptied list
* @list: the newl list to add.
* @head: the place to add it in the first list.
*
* Each of the lists is a queue.
* The list at @list is reinitialised
*/
extern void list_splice_tail_init(struct list_head *list,
	struct list_head *head);

/**
* list_entry - get the struct for this entry
* @ptr:	the &struct list_head pointer.
* @type:	the type of the struct this is embedded in.
* @member:	the name of the list_head within the struct.
*/
#define list_entry(ptr, type, member) \
	container_of(ptr, type, member)

/**
* list_first_entry - get the first element from a list
* @ptr:	the list head to take the element from.
* @type:	the type of the struct this is embedded in.
* @member:	the name of the list_head within the struct.
*
* Note, that list is expected to be not empty.
*/
#define list_first_entry(ptr, type, member) \
	list_entry((ptr)->next, type, member)

/**
* list_last_entry - get the last element from a list
* @ptr:	the list head to take the element from.
* @type:	the type of the struct this is embedded in.
* @member:	the name of the list_head within the struct.
*
* Note, that list is expected to be not empty.
*/
#define list_last_entry(ptr, type, member) \
	list_entry((ptr)->prev, type, member)

/**
* list_first_entry_or_null - get the first element from a list
* @ptr:	the list head to take the element from.
* @type:	the type of the struct this is embedded in.
* @member:	the name of the list_head within the struct.
*
* Note that if the list is empty, it returns NULL.
*/
#define list_first_entry_or_null(ptr, type, member) ({ \
	struct list_head *head__ = (ptr); \
	struct list_head *pos__ = READ_ONCE_LIST(head__->next); \
	pos__ != head__ ? list_entry(pos__, type, member) : NULL; \
})

/**
* list_next_entry - get the next element in list
* @pos:	the type * to cursor
* @member:	the name of the list_head within the struct.
*/
#define list_next_entry(type, pos, member) \
	list_entry((pos)->member.next, type, member)

/**
* list_prev_entry - get the prev element in list
* @pos:	the type * to cursor
* @member:	the name of the list_head within the struct.
*/
#define list_prev_entry(type, pos, member) \
	list_entry((pos)->member.prev, type), member)

/**
* list_for_each	-	iterate over a list
* @pos:	the &struct list_head to use as a loop cursor.
* @head:	the head for your list.
*/
#define list_for_each(pos, head) \
	for (pos = (head)->next; pos != (head); pos = pos->next)

/**
* list_for_each_prev	-	iterate over a list backwards
* @pos:	the &struct list_head to use as a loop cursor.
* @head:	the head for your list.
*/
#define list_for_each_prev(pos, head) \
	for (pos = (head)->prev; pos != (head); pos = pos->prev)

/**
* list_for_each_safe - iterate over a list safe against removal of list entry
* @pos:	the &struct list_head to use as a loop cursor.
* @n:		another &struct list_head to use as temporary storage
* @head:	the head for your list.
*/
#define list_for_each_safe(pos, n, head) \
	for (pos = (head)->next, n = pos->next; pos != (head); \
		pos = n, n = pos->next)

/**
* list_for_each_prev_safe - iterate over a list backwards safe against removal of list entry
* @pos:	the &struct list_head to use as a loop cursor.
* @n:		another &struct list_head to use as temporary storage
* @head:	the head for your list.
*/
#define list_for_each_prev_safe(pos, n, head) \
	for (pos = (head)->prev, n = pos->prev; \
	     pos != (head); \
	     pos = n, n = pos->prev)

/**
* list_for_each_entry	-	iterate over list of given type
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*/
#define list_for_each_entry(type, pos, head, member)				\
	for (pos = list_first_entry(head, type, member);	\
	     &pos->member != (head);					\
	     pos = list_next_entry(type, pos, member))

/**
* list_for_each_entry_reverse - iterate backwards over list of given type.
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*/
#define list_for_each_entry_reverse(type, pos, head, member)			\
	for (pos = list_last_entry(head, type, member);		\
	     &pos->member != (head); 					\
	     pos = list_prev_entry(type, pos, member))

/**
* list_prepare_entry - prepare a pos entry for use in list_for_each_entry_continue()
* @pos:	the type * to use as a start point
* @head:	the head of the list
* @member:	the name of the list_head within the struct.
*
* Prepares a pos entry for use as a start point in list_for_each_entry_continue().
*/
#define list_prepare_entry(type, pos, head, member) \
	((pos) ? : list_entry(head, type, member))

/**
* list_for_each_entry_continue - continue iteration over list of given type
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Continue to iterate over list of given type, continuing after
* the current position.
*/
#define list_for_each_entry_continue(type, pos, head, member) 		\
	for (pos = list_next_entry(type, pos, member);			\
	     &pos->member != (head);					\
	     pos = list_next_entry(type, pos, member))

/**
* list_for_each_entry_continue_reverse - iterate backwards from the given point
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Start to iterate over list of given type backwards, continuing after
* the current position.
*/
#define list_for_each_entry_continue_reverse(type, pos, head, member)		\
	for (pos = list_prev_entry(type, pos, member);			\
	     &pos->member != (head);					\
	     pos = list_prev_entry(type, pos, member))

/**
* list_for_each_entry_from - iterate over list of given type from the current point
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Iterate over list of given type, continuing from current position.
*/
#define list_for_each_entry_from(type, pos, head, member) 			\
	for (; &pos->member != (head);					\
	     pos = list_next_entry(type, pos, member))

/**
* list_for_each_entry_from_reverse - iterate backwards over list of given type
*                                    from the current point
* @pos:	the type * to use as a loop cursor.
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Iterate backwards over list of given type, continuing from current position.
*/
#define list_for_each_entry_from_reverse(type, pos, head, member)		\
	for (; &pos->member != (head);					\
	     pos = list_prev_entry(type, pos, member))

/**
* list_for_each_entry_safe - iterate over list of given type safe against removal of list entry
* @pos:	the type * to use as a loop cursor.
* @n:		another type * to use as temporary storage
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*/
#define list_for_each_entry_safe(type, pos, n, head, member)			\
	for (pos = list_first_entry(head, type, member),	\
		n = list_next_entry(type,pos, member);			\
	     &pos->member != (head); 					\
	     pos = n, n = list_next_entry(type, n, member))

/**
* list_for_each_entry_safe_continue - continue list iteration safe against removal
* @pos:	the type * to use as a loop cursor.
* @n:		another type * to use as temporary storage
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Iterate over list of given type, continuing after current point,
* safe against removal of list entry.
*/
#define list_for_each_entry_safe_continue(type, pos, n, head, member) 		\
	for (pos = list_next_entry(type, pos, member), 				\
		n = list_next_entry(type, pos, member);				\
	     &pos->member != (head);						\
	     pos = n, n = list_next_entry(type, n, member))

/**
* list_for_each_entry_safe_from - iterate over list from current point safe against removal
* @pos:	the type * to use as a loop cursor.
* @n:		another type * to use as temporary storage
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Iterate over list of given type from current point, safe against
* removal of list entry.
*/
#define list_for_each_entry_safe_from(type, pos, n, head, member) 			\
	for (n = list_next_entry(type, pos, member);					\
	     &pos->member != (head);						\
	     pos = n, n = list_next_entry(type, n, member))

/**
* list_for_each_entry_safe_reverse - iterate backwards over list safe against removal
* @pos:	the type * to use as a loop cursor.
* @n:		another type * to use as temporary storage
* @head:	the head for your list.
* @member:	the name of the list_head within the struct.
*
* Iterate backwards over list of given type, safe against removal
* of list entry.
*/
#define list_for_each_entry_safe_reverse(type,pos, n, head, member)		\
	for (pos = list_last_entry(head, type, member),		\
		n = list_prev_entry(type, pos, member);			\
	     &pos->member != (head); 					\
	     pos = n, n = list_prev_entry(type, n, member))

/**
* list_safe_reset_next - reset a stale list_for_each_entry_safe loop
* @pos:	the loop cursor used in the list_for_each_entry_safe loop
* @n:		temporary storage used in list_for_each_entry_safe
* @member:	the name of the list_head within the struct.
*
* list_safe_reset_next is not safe to use in general if the list may be
* modified concurrently (eg. the lock is dropped in the loop body). An
* exception to this is if the cursor element (pos) is pinned in the list,
* and list_safe_reset_next is called after re-taking the lock and before
* completing the current iteration of the loop body.
*/
#define list_safe_reset_next(type, pos, n, member)				\
	n = list_next_entry(type, pos, member)

#ifdef __cplusplus
}
#endif  /* __cplusplus */
#endif // _LIST_H
