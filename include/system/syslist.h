#ifndef _SYS_LIST_H
#define _SYS_LIST_H

#include <system/list.h>

#if defined(TARGET_LINUX32) || defined(TARGET_LINUX64)
#include <pthread.h>
typedef pthread_mutex_t slists_lock_t;
#elif defined(__XCC__) && defined(XOS_MUTEX_LOCK_USED)
#include <xtensa/xos.h>
typedef XosMutex slists_lock_t;
#else
typedef unsigned int slists_lock_t;
#endif

struct slists_group
{
    struct list_head group, free;
	slists_lock_t lock;
} PACKED;

typedef struct slists_group slists_group_t, *pslists_group_t;

struct slists
{
    struct list_head node;
    pslists_group_t group;
} PACKED;

typedef struct slists slists_t, *pslists_t;

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/**
 * slists_group_init
 */
ARCHLIB_API void slists_group_init(pslists_group_t psg);

/**
 * slists_group_deinit
 */
ARCHLIB_API void slists_group_deinit(pslists_group_t psg);

/**
 * slists_init
 */
ARCHLIB_API void slists_init(pslists_t psl);

/**
 *  slists_free
 */
ARCHLIB_API int slists_free(pslists_t psl);

/**
 *  slists_put
 */
ARCHLIB_API int slists_put(pslists_t  psl);

/**
 *  slists_alloc
 */
ARCHLIB_API pslists_t slists_alloc(pslists_group_t psg);

/**
 *  slists_get
 */
ARCHLIB_API pslists_t slists_get(pslists_group_t psg);

/**
 *  slists_chktop
 */
ARCHLIB_API pslists_t slists_chktop(pslists_group_t psg);

/**
 *  slists_freenum
 */
ARCHLIB_API int slists_freenum(pslists_group_t psg);

/**
 *  slists_topnum
 */
ARCHLIB_API int slists_topnum(pslists_group_t psg);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif // _SYS_LIST_H
