/*
 * This file was transplanted with slight modifications from Linux sources
 * (fs/cifs/md5.h) into U-Boot by Bartlomiej Sieka <tur@semihalf.com>.
 */

#ifndef _MD5_H
#define _MD5_H

#include <platform.h>

#define MD5_DIGEST_SIZE		16
#define MD5_HMAC_BLOCK_SIZE	64
#define MD5_BLOCK_WORDS		16
#define MD5_HASH_WORDS		4

struct MD5Context {
	uint32_t buf[MD5_HASH_WORDS];
	uint32_t bits[2];
	uint8_t in[64];
};

#ifdef __cplusplus
extern "C" {
#endif

	/*
	 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
	 * initialization constants.
	 */
	
		void MD5Init(struct MD5Context *ctx);

	/*
	 * Update context to reflect the concatenation of another buffer full
	 * of bytes.
	 */
	
		void MD5Update(struct MD5Context *ctx, const uint8_t *buf, unsigned len);

	/*
	 * Final wrapup
	 */
	
		void MD5Final(struct MD5Context *ctx, uint8_t digest[MD5_DIGEST_SIZE]);

	/*
	 * Calculate and store in 'output' the MD5 digest of 'len' bytes at
	 * 'input'. 'output' must have enough space to hold 16 bytes.
	 */
	
		void md5(uint8_t *input, int len, uint8_t output[MD5_DIGEST_SIZE]);

#ifdef __cplusplus
}// extern "C" 
#endif

#endif /* _MD5_H */
