----
# make a mp4 file of 1s duration  
ffmpeg -ss 7 -t 1 -i in.mp4 -c:a copy -c:v copy out.mp4
----
# make mkv  
ffmpeg -i in.mkv -c:a copy -c:v copy out.mkv  
----
# make pcm  
ffmpeg -i in.mp4 -vn -ar 44100 -ac 2 -f s16le out.pcm  
ffplay -ac 2 -ar 44100 -f s16le out.pcm  
----
# make yuv  
ffmpeg -i in.mp4 -s 640*480 -pix_fmt yuv420p out.yuv
ffplay -video_size 640*480 -i out.yuv
----
# make h264
ffmpeg -i in.mp4 -vcodec copy -bsf h264_mp4toannexb -f h264 out.h264  
----
# make aac  
ffmpeg -i in.mp4 -c:a copy out.aac  

