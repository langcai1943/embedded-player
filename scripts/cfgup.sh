#!/bin/sh
#

config_dir=./configs
if [ "$1" != "" ]; then
  config_dir=$1
fi;

config_files=$(find $config_dir -name *.cfg)

for s in $config_files; do
  echo "####  Auto generate '$s' .. "
  /bin/cp -rf $s ./.config;
  make scripts;
  /bin/cp -rf ./.config $s;
  incfile=`eval "dirname $s"`
  inh=$incfile/include/autoconf.h
  if [ -e $inh ]; then
      /bin/cp -rf ./generated/include/autoconf.h $inh
  fi;
  inh=$incfile/include/asm/autoconf.inc
  if [ -e $inh ]; then
      /bin/cp -rf ./generated/include/asm/autoconf.inc $inh
  fi;
done;
