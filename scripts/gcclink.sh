#!/bin/bash
#

# force directory
scripts_dir=`dirname $0`

KSCRIPTS_DIR()
{
    echo $scripts_dir
}

# get elf name
elffullname=${1}
elfname=${elffullname%%.elf*}

hbuild_env_name=$2
if [ "$hbuild_env_name" == "" ]; then
    hbuild_env_name=${elfname##*\/}
fi

if [ "x$elfname" == "x" ]; then
    return 2;
fi

# We need access to CONFIG_ symbols
auto_config_file=`readlink -f ${KSCRIPTS_DIR}/../generated/include/config/auto.conf`
if [ -e $auto_config_file ]; then
    . $auto_config_file
fi;

# check CROSS COMPILE tools
if [ "x$CROSS_COMPILE" == "x" ]; then
    CROSS_COMPILE=$CONFIG_CROSS_COMPILE
fi
if [ "x$CROSS_COMPILE" != "x" ]; then
if [ "x$NM" == "x" ]; then
    NM=`which ${CROSS_COMPILE}nm`
fi
if [ "x$OBJCOPY" == "x" ]; then
    OBJCOPY=`which ${CROSS_COMPILE}objcopy`
fi
fi

# Error out on error
set -e

# Nice output in kbuild format
# Will be supressed by "make -s"
info()
{
    if [ "${quiet}" != "silent_" ]; then
        printf "  %-7s %s\n" ${1} ${2}
    fi
}

# Link of elf
# ${1} - optional extra .o files
# ${2} - output file
eval_get()
{
    eval "echo \$$1"
}

elf_link()
{
    info ELF ${1}

    local helf_lds=$(eval_get HBUILD_ELF_LDS_${hbuild_env_name})
    local helf_init=$(eval_get HBUILD_ELF_INIT_${hbuild_env_name})
    local helf_main=$(eval_get HBUILD_ELF_MAIN_${hbuild_env_name})
    local helf_libs=$(eval_get HBUILD_ELF_LIBS_${hbuild_env_name})

    if [ "$helf_main" == "" ]; then
        helf_main=${elfname}.o
    fi;

    local helf_cmd=""

    if [ "$CONFIG_GCC" == "y" ]; then

        local pure_elfname=${elfname##*\/}
        local helf_lpath=${elfname%\/*}

        if [ "$helf_lds" == "" ]; then
            if [ "$HBUILD_ELF_LDS_NAME" != "" ]; then
                helf_lds=${HBUILD_ELF_LDS_NAME}
            else
                helf_lds=${pure_elfname}.lds
            fi;
        fi;

        if [ "$pure_elfname" != "$elfname" ]; then
            helf_lds=$helf_lpath/$helf_lds
        fi;

        if [ -e ${helf_lds} ] && [ "x$CROSS_COMPILE" != "x" ]; then

            local all_objs="";
            for add_obj in $helf_libs; do
                if [ -e $add_obj ]; then
                    all_objs=`echo $all_objs $add_obj`;
                fi;
            done;

            helf_cmd="${LD} ${LDFLAGS} ${HBUILD_LDFLAGS} -o ${1} \
                -T ${helf_lds} ${helf_init}            \
                --start-group ${helf_main} --end-group \
                $all_objs -Map ${1}.map"
        else

            local all_objs="";
            for add_obj in $helf_main $helf_libs; do
                if [ -e $add_obj ]; then
                    all_objs=`echo $all_objs $add_obj`;
                fi;
            done;

            if [ "x$CROSS_COMPILE" == "x" ]; then
                helf_cmd="${CC} ${CFLAGS} ${HBUILD_CFLAGS} -o ${1} \
                    $all_objs -m32 -lpthread -lm -lcurses -lrt -lstdc++ -L/usr/lib/i386-linux-gnu -lSDL2 -lavutil -lavformat -lavcodec -lavutil -lswscale -lswresample -lm -lpthread -lz `pkg-config --cflags --libs sdl2`"
#                    $all_objs -m32 -lpthread -lm -lcurses -lrt -lstdc++"
            else
                helf_cmd="${LD} ${LDFLAGS} ${HBUILD_LDFLAGS} -o ${1} \
                    $all_objs -Map ${1}.map"
            fi;
        fi;

    fi;

    if [ "x$helf_cmd" != "x" ]; then
        if [ "${V}" != "" ]; then
            echo ${helf_cmd}
        fi;
        eval "${helf_cmd}"
    fi;

}

# Create binary file with elf file from ${1}
mkbinary()
{
    info BIN $2
    $OBJCOPY --remove-section=.reginfo -O binary $1 $2
}

# Create hex file with bin file for flash from ${1}
makehex()
{
    info HEX16 ${2}16
    /bin/hexdump -v -e '1/2 "%04X" "\n"' $1 > ${2}16

    info HEX32 ${2}32
    /bin/hexdump -v -e '1/4 "%08X" "\n"' $1 > ${2}32

    info M $3
    ${KSCRIPTS_DIR}/mem.py ${2}32 > $3
}

# Create src file with bin file for flash from ${1}
makescr()
{
    local setting=${HTOP_DIR}/platform/${CONFIG_SYS_CPU}/${CONFIG_SYS_CORE}/platform.set
    if [ -e ${setting} ]; then
        info SCR ${2}
        cat $1 ${setting} > ${2}
        makehex ${2} ${2}.hex ${2}.m
    fi;
}

#
#
# Use "make V=1" to debug this script
case "${HBUILD_VERBOSE}" in
*1*)
    set -x
    ;;
esac

#
elf_link $elffullname

#
if [ "${EXT_HBUILD_OUTPUT}" != "" ] && [ -d ${EXT_HBUILD_OUTPUT} ]; then
    test ! -e ${elfname}.elf       || /bin/cp -f ${elfname}.elf        ${EXT_HBUILD_OUTPUT}
fi
