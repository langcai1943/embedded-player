# Backward compatibility/*
# This file is free ; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program;
#*/
empty :=

asflags-y  += $(EXTRA_AFLAGS)
ccflags-y  += $(EXTRA_CFLAGS)
cppflags-y += $(EXTRA_CPPFLAGS)
ldflags-y  += $(EXTRA_LDFLAGS)

#
# flags that take effect in sub directories
export HBUILD_SUBDIR_ASFLAGS := $(HBUILD_SUBDIR_ASFLAGS) $(subdir-asflags-y)
export HBUILD_SUBDIR_CCFLAGS := $(HBUILD_SUBDIR_CCFLAGS) $(subdir-ccflags-y)

# Figure out what we need to build from the various variables
# ===========================================================================

# When an object is listed to be built compiled-in and modular,
# only build the compiled-in version

obj-m := $(filter-out $(obj-y),$(obj-m))

# Libraries are always collected in one lib file.
# Filter out objects already built-in
ifneq ($(strip $(lib-target)),$(empty))
ifeq ($(strip $(lib-y)),$(empty))
lib-y := $(sort $(obj-y) $(obj-m))
else
lib-y := $(filter-out $(obj-y), $(sort $(lib-y) $(lib-m)))
endif
endif

# Handle objects in subdirs
# ---------------------------------------------------------------------------
# o if we encounter foo/ in $(obj-y), replace it by foo/built-in.o
#   and add the directory to the list of dirs to descend into: $(subdir-y)
# o if we encounter foo/ in $(obj-m), remove it from $(obj-m) 
#   and add the directory to the list of dirs to descend into: $(subdir-m)

# Determine modorder.
# Unfortunately, we don't have information about ordering between -y
# and -m subdirs.  Just put -y's first.
modorder	:= $(patsubst %/,%/modules.order, $(filter %/, $(obj-y)) $(obj-m:.o=.ko))

__subdir-y	:= $(patsubst %/,%,$(filter %/, $(obj-y)))
subdir-y	+= $(__subdir-y)
__subdir-m	:= $(patsubst %/,%,$(filter %/, $(obj-m)))
subdir-m	+= $(__subdir-m)
obj-y		:= $(patsubst %/, %/built-in.o, $(obj-y))
obj-m		:= $(filter-out %/, $(obj-m))

# Subdirectories we need to descend into
subdir-libs-ym := $(subdir-libs-y) $(subdir-libs-m)
subdir-ym	:= $(subdir-libs-ym) $(subdir-y) $(subdir-m)

# if $(foo-objs) exists, foo.o is a composite object 
multi-used-y := $(sort $(foreach m,$(obj-y), $(if $(strip $($(m:.o=-objs)) $($(m:.o=-y))), $(m))))
multi-used-m := $(sort $(foreach m,$(obj-m), $(if $(strip $($(m:.o=-objs)) $($(m:.o=-y))), $(m))))
multi-used   := $(multi-used-y) $(multi-used-m)
single-used-m := $(sort $(filter-out $(multi-used-m),$(obj-m)))

# Build list of the parts of our composite objects, our composite
# objects depend on those (obviously)
multi-objs-y := $(foreach m, $(multi-used-y), $($(m:.o=-objs)) $($(m:.o=-y)))
multi-objs-m := $(foreach m, $(multi-used-m), $($(m:.o=-objs)) $($(m:.o=-y)))
multi-objs   := $(multi-objs-y) $(multi-objs-m)

# $(subdir-obj-y) is the list of objects in $(obj-y) which uses dir/ to
# tell hbuild to descend
subdir-obj-y := $(filter %/built-in.o, $(obj-y))

# $(obj-dirs) is a list of directories that contain object files
obj-dirs := $(dir $(multi-objs) $(obj-y))

# Replace multi-part objects by their individual parts, look at local dir only
real-objs-y := $(foreach m, $(filter-out $(subdir-obj-y), $(obj-y)), $(if $(strip $($(m:.o=-objs)) $($(m:.o=-y))),$($(m:.o=-objs)) $($(m:.o=-y)),$(m))) $(extra-y)
real-objs-m := $(foreach m, $(obj-m), $(if $(strip $($(m:.o=-objs)) $($(m:.o=-y))),$($(m:.o=-objs)) $($(m:.o=-y)),$(m)))

# Add subdir path

extra-y		:= $(addprefix $(obj)/,$(extra-y))
always		:= $(addprefix $(obj)/,$(always))
targets		:= $(addprefix $(obj)/,$(targets))
modorder	:= $(addprefix $(obj)/,$(modorder))
obj-y		:= $(addprefix $(obj)/,$(obj-y))
obj-m		:= $(addprefix $(obj)/,$(obj-m))
lib-y		:= $(addprefix $(obj)/,$(lib-y))
subdir-obj-y	:= $(addprefix $(obj)/,$(subdir-obj-y))
real-objs-y	:= $(addprefix $(obj)/,$(real-objs-y))
real-objs-m	:= $(addprefix $(obj)/,$(real-objs-m))
single-used-m	:= $(addprefix $(obj)/,$(single-used-m))
multi-used-y	:= $(addprefix $(obj)/,$(multi-used-y))
multi-used-m	:= $(addprefix $(obj)/,$(multi-used-m))
multi-objs-y	:= $(addprefix $(obj)/,$(multi-objs-y))
multi-objs-m	:= $(addprefix $(obj)/,$(multi-objs-m))
subdir-ym	:= $(addprefix $(obj)/,$(subdir-ym))
obj-dirs	:= $(addprefix $(obj)/,$(obj-dirs))

# These flags are needed for modversions and compiling, so we define them here
# already
# $(modname_flags) #defines HBUILD_MODNAME as the name of the module it will
# end up in (or would, if it gets compiled in)
# Note: Files that end up in two or more modules are compiled without the
#       HBUILD_MODNAME definition. The reason is that any made-up name would
#       differ in different configs.
name-fix = $(subst $(comma),_,$(subst -,_,$1))
basename_flags = -D"HBUILD_BASENAME=HBUILD_STR($(call name-fix,$(basetarget)))"
modname_flags  = $(if $(filter 1,$(words $(modname))),\
                 -D"HBUILD_MODNAME=HBUILD_STR($(call name-fix,$(modname)))")

orig_c_flags   = $(HBUILD_CPPFLAGS) $(HBUILD_CFLAGS) $(HBUILD_SUBDIR_CCFLAGS) \
                 $(ccflags-y) $(CFLAGS_$(basetarget).o)
_c_flags       = $(filter-out $(CFLAGS_REMOVE_$(basetarget).o), $(orig_c_flags))
_a_flags       = $(HBUILD_AFLAGS) $(HBUILD_SUBDIR_ASFLAGS) \
                 $(asflags-y) $(AFLAGS_$(basetarget).o)
_cpp_flags     = $(HBUILD_CPPFLAGS) $(cppflags-y) $(CPPFLAGS_$(@F))

#
# If building the kernel in a separate objtree expand all occurrences
# of -Idir to -I$(srctree)/dir except for absolute paths (starting with '/').

ifeq ($(HBUILD_SRC),$(empty))
__c_flags	= $(_c_flags)
__a_flags	= $(_a_flags)
__cpp_flags     = $(_cpp_flags)
else

# -I$(obj) locates generated .h files
# $(call addtree,-I$(obj)) locates .h files in srctree, from generated .c files
#   and locates generated .h files
# FIXME: Replace both with specific CFLAGS* statements in the makefiles
__c_flags	= $(call addtree,-I$(obj)) $(call flags,_c_flags)
__a_flags	=                          $(call flags,_a_flags)
__cpp_flags     =                          $(call flags,_cpp_flags)
endif

c_flags        = -Wp,-MD,$(depfile) $(NOSTDINC_FLAGS) $(HBUILD_INCLUDE)     \
		 $(__c_flags) $(modkern_cflags)                           \
		 -D"HBUILD_STR(s)=\#s" $(basename_flags) $(modname_flags)

ifeq ($(CONFIG_GCC),y)
a_flags        = -Wp,-MD,$(depfile) $(NOSTDINC_FLAGS) $(HBUILD_INCLUDE)     \
		 $(__a_flags) $(modkern_aflags)
endif

ifeq ($(CONFIG_SDCC),y)
a_flags        = $(NOSTDINC_FLAGS) $(HBUILD_INCLUDE)     \
		 $(__a_flags) $(modkern_aflags)
endif

ifeq ($(CONFIG_GXCC),y)
a_flags        = -Wp,-MD,$(depfile) $(NOSTDINC_FLAGS) $(HBUILD_INCLUDE)     \
		 $(__a_flags) $(modkern_aflags)
endif

cpp_flags      = -Wp,-MD,$(depfile) $(NOSTDINC_FLAGS) $(HBUILD_INCLUDE)     \
		 $(__cpp_flags)

ld_flags       = $(LDFLAGS) $(ldflags-y)

# Finds the multi-part object the current object will be linked into
modname-multi = $(sort $(foreach m,$(multi-used),\
		$(if $(filter $(subst $(obj)/,,$*.o), $($(m:.o=-objs)) $($(m:.o=-y))),$(m:.o=))))

# GPERF
# ---------------------------------------------------------------------------
quiet_cmd_gperf = GPERF $@
      cmd_gperf = gperf -t --output-file $@ -a -C -E -g -k 1,3,$$ -p -t $<

.PRECIOUS: $(src)/%.hash.c_shipped
$(src)/%.hash.c_shipped: $(src)/%.gperf
	$(call cmd,gperf)

# LEX
# ---------------------------------------------------------------------------
LEX_PREFIX = $(if $(LEX_PREFIX_${baseprereq}),$(LEX_PREFIX_${baseprereq}),yy)

quiet_cmd_flex = LEX     $@
      cmd_flex = flex -o$@ -L -P $(LEX_PREFIX) $<

.PRECIOUS: $(src)/%.lex.c_shipped
$(src)/%.lex.c_shipped: $(src)/%.l
	$(call cmd,flex)

# YACC
# ---------------------------------------------------------------------------
YACC_PREFIX = $(if $(YACC_PREFIX_${baseprereq}),$(YACC_PREFIX_${baseprereq}),yy)

quiet_cmd_bison = YACC    $@
      cmd_bison = bison -o$@ -t -l -p $(YACC_PREFIX) $<

.PRECIOUS: $(src)/%.tab.c_shipped
$(src)/%.tab.c_shipped: $(src)/%.y
	$(call cmd,bison)

quiet_cmd_bison_h = YACC    $@
      cmd_bison_h = bison -o/dev/null --defines=$@ -t -l -p $(YACC_PREFIX) $<

.PRECIOUS: $(src)/%.tab.h_shipped
$(src)/%.tab.h_shipped: $(src)/%.y
	$(call cmd,bison_h)

# Shipped files
# ===========================================================================

quiet_cmd_shipped = SHIPPED $@
cmd_shipped = cat $< > $@

$(obj)/%: $(src)/%_shipped
	$(call cmd,shipped)

# Commands useful for building a boot image
# ===========================================================================
# 
#	Use as following:
#
#	target: source(s) FORCE
#		$(if_changed,ld/objcopy/gzip)
#
#	and add target to extra-y so that we know we have to
#	read in the saved command line

# Linking
# ---------------------------------------------------------------------------

quiet_cmd_ld = LD      $@
cmd_ld = $(LD) $(LDFLAGS) $(ldflags-y) $(LDFLAGS_$(@F)) \
	       $(filter-out FORCE,$^) -o $@ 

# Objcopy
# ---------------------------------------------------------------------------

quiet_cmd_objcopy = OBJCOPY $@
cmd_objcopy = $(OBJCOPY) $(OBJCOPYFLAGS) $(OBJCOPYFLAGS_$(@F)) $< $@

# Gzip
# ---------------------------------------------------------------------------

quiet_cmd_gzip = GZIP    $@
cmd_gzip = (cat $(filter-out FORCE,$^) | gzip -n -f -9 > $@) || \
	(rm -f $@ ; false)

# Bzip2
# ---------------------------------------------------------------------------

# Bzip2 and LZMA do not include size in file... so we have to fake that;
# append the size as a 32-bit littleendian number as gzip does.
size_append = printf $(shell						\
dec_size=0;								\
for F in $1; do								\
	fsize=$$(stat -c "%s" $$F);					\
	dec_size=$$(expr $$dec_size + $$fsize);				\
done;									\
printf "%08x\n" $$dec_size |						\
	sed 's/\(..\)/\1 /g' | {					\
		read ch0 ch1 ch2 ch3;					\
		for ch in $$ch3 $$ch2 $$ch1 $$ch0; do			\
			printf '%s%03o' '\\' $$((0x$$ch)); 		\
		done;							\
	}								\
)

quiet_cmd_bzip2 = BZIP2   $@
cmd_bzip2 = (cat $(filter-out FORCE,$^) | \
	bzip2 -9 && $(call size_append, $(filter-out FORCE,$^))) > $@ || \
	(rm -f $@ ; false)

# Lzma
# ---------------------------------------------------------------------------

quiet_cmd_lzma = LZMA    $@
cmd_lzma = (cat $(filter-out FORCE,$^) | \
	lzma -9 && $(call size_append, $(filter-out FORCE,$^))) > $@ || \
	(rm -f $@ ; false)

quiet_cmd_lzo = LZO     $@
cmd_lzo = (cat $(filter-out FORCE,$^) | \
	lzop -9 && $(call size_append, $(filter-out FORCE,$^))) > $@ || \
	(rm -f $@ ; false)

quiet_cmd_lz4 = LZ4     $@
cmd_lz4 = (cat $(filter-out FORCE,$^) | \
	lz4c -l -c1 stdin stdout && $(call size_append, $(filter-out FORCE,$^))) > $@ || \
	(rm -f $@ ; false)

# XZ
# ---------------------------------------------------------------------------
# Use xzkern to compress the kernel image and xzmisc to compress other things.
#
# xzkern uses a big LZMA2 dictionary since it doesn't increase memory usage
# of the kernel decompressor. A BCJ filter is used if it is available for
# the target architecture. xzkern also appends uncompressed size of the data
# using size_append. The .xz format has the size information available at
# the end of the file too, but it's in more complex format and it's good to
# avoid changing the part of the boot code that reads the uncompressed size.
# Note that the bytes added by size_append will make the xz tool think that
# the file is corrupt. This is expected.
#
# xzmisc doesn't use size_append, so it can be used to create normal .xz
# files. xzmisc uses smaller LZMA2 dictionary than xzkern, because a very
# big dictionary would increase the memory usage too much in the multi-call
# decompression mode. A BCJ filter isn't used either.
quiet_cmd_xzkern = XZKERN  $@
cmd_xzkern = (cat $(filter-out FORCE,$^) | \
	sh $(KSCRIPTS_DIR)/xz_wrap.sh && \
	$(call size_append, $(filter-out FORCE,$^))) > $@ || \
	(rm -f $@ ; false)

quiet_cmd_xzmisc = XZMISC  $@
cmd_xzmisc = (cat $(filter-out FORCE,$^) | \
	xz --check=crc32 --lzma2=dict=1MiB) > $@ || \
	(rm -f $@ ; false)
