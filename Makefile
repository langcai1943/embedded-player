#
# Makefile
#/*
# This file is free ; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program;
#*/

# here include core make
ifndef KSCRIPTS_DIR
LOCAL_TO_HTOP_DIR := .
KSCRIPTS_DIR := $(PWD)/$(LOCAL_TO_HTOP_DIR)/scripts
include $(KSCRIPTS_DIR)/Makefile.target
endif

#
#

subdir-y += scripts/
subdir-y += complier/
subdir-y += arch/
subdir-y += lib/
subdir-y += fs/
subdir-y += platform/
subdir-y += user/

#
#
#

ifdef LOCAL_TO_HTOP_DIR

include $(KSCRIPTS_DIR)/Makefile.subtop

else

obj-y	+= $(subdir-y)

endif

PHONY += FORCE
FORCE:

# Declare the contents of the .PHONY variable as phony.  We keep that
# information in a variable se we can use it in if_changed and friends.

.PHONY: $(PHONY)
